Cypress.Commands.add("unregisterServiceWorkers", () => {
	cy.log("unregisterServiceWorkers");
	if ("serviceWorker" in navigator) {
		navigator.serviceWorker.getRegistrations().then((registrations) => registrations.forEach((reg) => reg.unregister()));
	}
});