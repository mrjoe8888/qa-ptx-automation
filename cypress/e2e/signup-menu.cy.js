/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-07-14
 * @LastEditTime: 
 * @Description: Basic sign up
 **/
/// <reference types="cypress" />

import Faker from 'faker';



describe("Promotexter\'s sign up testing", function() {  
    
    beforeEach(() => {
        cy.visit('/signup');
    })
    it("Verify the Promotexter logo should be correct", function() {  
        /*
        * Download the image from the source
        * https://tamstorageqa.z16.web.core.windows.net/ms-icon-70x70.png
        * convert it with the online converter
        * image to base64
        * https://base64.guru/converter/encode/image
        * 
        */
        const tamLogoBase64 = "iVBORw0KGgoAAAANSUhEUgAAAOEAAAAtCAYAAABYmQy1AAAOKklEQVR42u2de5AUxR3HO0GhfGBc8cFDhZs7OI4777jbu1Mx5g+zZR6aqEmuTKVS8ZVsJaUxsVI6KXkc71sinHAGzSqKoEiycoJERR1FQMTXKuIjkpgtIg9FxT3AN3D9S8/sDvy2t3u2527u9q7sq/rWLmz3zG9n+jO/X3f/upcAACHtHbl6yFOnMl3OdDvTs0zbmNJZvcu0iSnOjvNL9jrc81jceW1btLS+biI+IGxgWsz0GRMo6iumB5jO1xBqaXUdwhDT3T7Ak2l5nmfUEGppFYTwQqYPAwDQ1SdMP9EQammpQfjrAOHj9ScNoZaWN4RXekLUngby8D4g1ldA1rMDPJuV/d46wD7bnynjDeINGkItLRfC3D7gBCk4Kz4GspoBtpFVeuoADFy8gx7/l9fpic0vQmjKCzB4zhY68O53KXnyK0qeY2X++UmmjhzEi/C5CxgbZbJ8ymQy9I3W6k+e8CgGw14pgM90OvrW7M3UuOw+WlU/l46vmEFrx0wDW/b7qvAttPTHS2ho+svUBpWsp6yu1CseZDpBEUIbqK7+xfTN1uovnvBOMYBpx/sNeHAPNS5dSsOjJkFdWTMw4KDynFthXFb2ewYm1I2eCuGRE6Hsh/fQo5ftznhFeXi6shcg1CBq9QtPeIYQkgc/dgActGQnraqdQ224HPAmLIBx584Xi31WyV7DJZOhumoWPebObRkQ5R6xqgsQpiUhaNIDRB2adk9G9j640nYHCuFDHXfJQtABiT0OgLb3q/h2mxw+TnbZ2vJpUF05kw687z2aCU2FfcRVXYDQ8igbYooLIDQ1SN1SpJ8+1Pq03W44Oojpyzw47EEYBmHJJUtpPfOA4zgAK7Ovo+vnQVl4HlSg/zv8OasTLpkEZd+7izqjqY98Khlx7TgpQAhdpTWEPdqYo9ru4DzhxcJpCBaGDp71qtMHdEJQBFjN+W1wetUcGHjaZBg6djYdNraFDho6GYaPa3E+w5COmzAf6s+8GUKTNlGySdo//FUPQGh5QIhD2CbkQe0yCY8bFc6WkY3GhhVschXhGkocfSazIapYjvSA/XxjTnH1YwXqxgTnjKHrz4svn8jeI+KzfG/b7at9uZ5wbh4Uq/Y70xAll97nhKG4D1jzndsgZMyAkdV/oa0LN9Lk5p2dr27Z1blw0fOd5Q2t9ISR05wyuI9oh6VlP1hEyWOfZzxsPoSLewDClAeE+C+evdhpD68Zyl48lT+vxsLbYwgeFvgviRpSSqGcV4jeXfsjBepZEugthXMmBQ8AQxDNJHwM3DUVyW5f7cuF8Kk8KKwDcNQ922lVeK497XDYs1UyrzasosUB8K2tH3TyFv1ve5qWN7bSU0bPgioEblXDPKiuboFBd/yXkrWHRBAmA4YwUqBPyA/yeJUNFRjw8QMF/0ROKxzLUjyn5QFgEPb7bcyG4vfD94Fv0E0KfftwgdHwYtit3L5cCP+TB8V6Foq2vEbHj52eG4qeMx8GnDyR3p/YTGUWPfrEVjrw1Em04uxbc/qHtaOnwonNL1InuyYfwh0BQhiWfHFDcpFENwWHeKJBnhQ34ibyUvECEPKN3lIADodSou8YEZwzKPtlDzZX0QLdAfd7migkVnmQxATlwh4PmGQfsFu5fbkQfpAHBQPlpJufo7WjmxmER/qCzAPCWObpOjo+l0J46GAnrb9gIR0xLpbTj6wzpsApNzwtg3BfABBGJA1O1KBAcmNCgqeiClyyxm4UOG9a0LcwJeVMhYeN2YP2ixqz14NQZXQ67AEYVlLwEBGNgqcF17wYdqu2r8MQ7hFBOOTPz9Lasqk5ENoDLw3fvZ0eOHCIeqF+wSWLnJC0MgfCyXDq9RZ10t7yIfy0GxBGC4QPotBKdfpCND8Z8gj7CkEh67eQLpaLF3g4BWm/n8ZsFvBOXt/BlMz1pQXHVLlOxbBbeXrMhfAjEYShiRudEBJDaE9HnDhqOn3jX7ulEL66ZRc9vSpGjbq5OeFoXekUONncIINwTzcgbCrg+kMKjTwiOW9C0YvIbkyii+dVLVcoQgjSfj+N2fIxPWQqDr40dTErqhh2q94/Dwg3ABzTvAFqynPDUXuw5ZjhU+Dya5bbEApBbItvouTYm+Cs844MzNj9yvFjZ4Cd8O2stsiH8N/dgDAi6fN4Dd2rXiQ/N0UFit6GMEj7u9OYUyBPtE/56O/HPKId0gMQdtXuACBcB/CNmWuhvGIi1DQuyPFotgYNnQLX3bSavr97vwsj3fnePvrSKzvghZe3wyjWdyxFnrC6xukf0m8u/5CSNV+IIHwsgMatOlenIex9CP38FRr5Tiv2I4tpdwAQrmUf3PI0DK03oX78AqiYgLwh83AVzLPZk/PsPb3quhWdV/++vdN+P6R0Jlz886Vw7oV3OKGrU+e8NmeyfsRVq2SDMraae2CeUEPYPyFMgPeSNr91imF3ABA+zT6IrYUBlzbDWY23Qk1da86EvT1faMN4xllz4NgRzY7s92MaWuG08tlQ3tjqfG7Xqa6ZAzWVM+nRi3dk5gjFGTMNfRRCv32qWB+DMEj7/TTmuE/4VbN9vAbgzAAgDMruACB8in0way2Q386D0NVLoWFMC1Q1tuatnqj0SuK2V1Oc3QqNw00YcuN6r5S1HQor64sFoSnoI3Q1S6cYEAZpf3dGGbt7v0TzgaphaTHsDgBCy/aE64Fc0wrkjvUw7Mq/Q+NI5hXrbmHh5YLCELIy9trChmE3wYgrVmYGY+zkbTGE1wa8iiLIixT28cQ1FY7b2xAGaX/ER1/M8DNMryDZfKBs/pAU2e4AIHzS7hNuBPKb24C0MRjb34HT/rCG1pVOhbrSZubhJGsKbe/H+ov2wt66UZNg2O/WULKOUvLEl7JlTPvcTZ/6KISyfkI8O2Qeyb7GFQcYehvCIO0Pe2TeRAQNOyYpH0N1oigTJS6ZZoh6zAeq5JcWw+4AIHyiE0jr80Cuv4u9rgOybCuQdfvhuL++Tct+dC8dXz6d2ot27cTsmspZjmrLpzsLeWvHTKOjv7+IDp73ZmYx75rPvfaauUxxo6diQug3l9B9Uof7CIRB2p/2MarZlZxVS6EfGFOYP4wW2e4AIHz8EJAFLwG5cSmQhQzGJW8zkHZmNnmyDsLx89+mQ699nJb8bBktvWgxlDGV/HQZtT2fA9/jX4AD4Mq9XivqEz42eiomhG5jUL0xKY9wpxgQBmm/6XNqIeQxt1foGCp5oTLvxT9EetPugCBccxDIba8AmfgPIPGXmSd8h4Wk72f6dKs/yWzgZANph5n20iRb9nt7CmKDu9Oa57aHb2Tg23tYfRxCHBolBAMYaVBb21csCIOy3z1G0uf8nrvFhCzxPInmeQv1A2XnEAEbKoLdAUH42AEgt28GMm0VkEWvAVm+jf3/RxnPhsvZ+4/awNmDLvZ7tc1/t7Lwc7Ded1RLywvCRxmEf9vyGZn9CJB73wSS2M4A/DgfQv96hum4vA2HNYRaGkIOloeZZ1v+/hTStvEXZMlbLLTcxQC0w8tuQThL9jsUGkItDaFoj5nVX8wk928jzBOWk/YP7icrO7oK4Qqm8V6/yKQh1NIQiuBZua+NhZ+EPMBAXLGbsPcV7P+n2ltRMNEC4L3GNJupRuW3CTWEWhpCIUjpGWTlfgbgR4Q8+B5xgDwC0plMFzBdwfTHrK5iijCNKvBrvxpCLS01CDuuzGwMzOBr38Ne06QgXKr6+kIYBvEi40LLr6KSuSwDjmyl58eGCHjvztaXds+OcPoaQdjeUZn7k2kawgAkWslgKMxVgmSOMIFyI1VzMN2FqilQW3vJZ6f05ibKFuQvrO2vP2dgCFLqPCHcpfhb9hpC/xDy+7zEOQjdCeIoVy8CuT/3Fkb5n8B5TVOSyJwS/DsEuXuzRAXHMuDIXpwpyN3T00S2u8fCNvBl8HeMcN6ZX5QtmggXnYN/SIS5/FLR71CYgvxR0TWOcPWjkP+ze3y9Jjiy0XIT5G5KZapC2BIodBpCDBPeesPdXCmFGkMKebk45O8ckOIyOTCE8Wy9mCBjRNQQo9n6lsDrxrLHMpHXxBC6GShm9jWKbHGTmlOojJtKhr9jEjXqdLZOIQgjKFE7hlLZTHSuFPKa+HrG0HHdf6fRQy/JfV9sl5tphOsRST0LHTfJJckrQtjeMUQEioYwEAhN5JHwtvYRQepYGq0CCCE4TAGEbrKz23dKcseyJKGkDMIE8jRNqO9pIdsMLtE6wuV3AipjcTBE0LUwJSsoLMHqD9k5cHjt2hnmVosASmjnr0saPUjiWWG7+EjCjU5E9fD29yayG1TD0alCADWEQUHoNqII8oAW94obCH/zTAQI/txtEKLfQeiKJ3RDKDevlIcQJN/NkvRlLVQfb3acAPnPlsk8oSU4L3+NLJQEjjdW5utbKAeX/20ObJfXefl6+D6akvsohfD1wPt/GkJRQ42ixkg4T2hyoarh0xOGUF9IlHjN9wlFnoVwA0g4bLQ4L00EDxQvCBMcWEZAEKY472NxnsxAnhBfT+wJcd82pAihqJ5vCN0duNMMhlOkXjBIb6ghDKEwBkNoIG+WQg0IUD8nzXklvk+IRz8NwehokuvbYW+R5I6V5I4VQX2dJmRrGoV9hSAMo3pJ7qGiMjoalcDQxF0jC/U/8RaFBPUlLdRFiHLXvkkRQlE9EYQhFFXkQZhd4b63BC8tkktD2M15QiLwVAY3+hcRlDW4+b0Q1ydTnQcMI+BF5zQKHAuXMQTH8nofknzHkMRe2TyhIbimYVQmhjw5ttFAQEUQWFHJ9+HtMjzuZYS7LkRwnwx+WsiF8AGmM/j1fRpCrX6maIEoQBYRJItp9/8BPu4PC9RI1p4AAAAASUVORK5CYII=";
       // check if image was loaded
        cy.get("img[src='/assets/images/pt3logo.png']")
            .should('be.visible')
            .and(($img) => { 
                expect($img[0].naturalWidth).to.be.equal(225)
        }).then(() => {
            imageToBase64Converter("https://portal.promotexter.com/assets/images/pt3logo.png", function (dataUrlImage) {
                expect(JSON.stringify(`data:image/png;base64,${tamLogoBase64}`)).to.be.equal(JSON.stringify(dataUrlImage));
            })   
        }) 
    })
    it("Verify the heading should be h4 and with text label 'Sign up for free'", function() { 
        cy.get("h4[class='title']").should('have.text',"Sign up for free")
    })
    it("Verify should accept input field for email addresss", function() { 
        const email = Faker.internet.email();
        cy.get("input[id='email']")
            .clear()
            .type(email)
            .invoke('val').should((input_email) => {
                expect(input_email).to.be.equals(email);
            })
    })
    it("Verify should prompt an error if inputting invalid email address", function() { 
  
        const name = Faker.name.findName();
        const username = Faker.internet.userName(name);
        const domain = Faker.internet.domainSuffix();
        const invalid_email = `${username}@.${domain}`;
        cy.get("input[id='email']")
            .type(invalid_email).then(() => {
                cy.get("div[id='errorEmail2']")
                .should('have.text', "Sorry this email address looks invalid. Please correct the address or try a different one.")
            })
        
    })
    it("Verify should display the tooltip if clicking the input field for Mobile Number", function() { 
        cy.get("input[id='su-mobile']")
        .clear()
        .click().then(() => {
            cy.get("input[id='su-mobile']")
                .invoke('attr', 'uib-popover')
                .should('eq', "We will send a PIN code to your mobile number. Use country code and number, for example: +447123456789");
        })
    })
    it("Verify should not display an error if area code is valid and the mobile number are valid inputs", function() { 
        const valid_mobile_no = Faker.phone.phoneNumber('+48##########')
        cy.get("input[id='su-mobile']")
            .type(valid_mobile_no)
            .invoke('val').should((mobile_no) => {
                    expect(mobile_no).to.be.equals(valid_mobile_no);
            })
    })
    it("Verify should shown up the actual password value if eye icon is click from the side corner from the password input field", function() { 
        const generate_password = Faker.internet.password(10);
        cy.get("#form-error-field > div.float-container > input")
            .type(generate_password)
            .then(() => {
                cy.get("#showPassword")
                .then(() => {
                    cy.get("i[class='mdi mdi-eye']")
                    .click()
                    .invoke('attr', 'class')
                    .should('eq', "mdi mdi-eye-off");
                })
            })
    })
    it("Verify should hide the actual password value if eye icon is click again from the side corner from the password input field ", function() { 
        const generate_password = Faker.internet.password(10);
        cy.get("#form-error-field > div.float-container > input")
            .type(generate_password)
            .then(() => {
                cy.get("#showPassword")
                .then(() => {
                    cy.get("i[class='mdi mdi-eye']")
                    .click()
                    .click()
                    .invoke('attr', 'class')
                    .should('eq', "mdi mdi-eye");
                })
            })
    })
    it("Verify should  thrown an error if input password less than 10 characters", function() { 
        const nine_random_strings = Faker.internet.password(9);
        cy.get("#form-error-field > div.float-container > input")
            .type(nine_random_strings)
            .then(() => {
                cy.get("#showPassword > div.row.password-checking > div:nth-child(2) > div > div:nth-child(2) > div")
                    .should('have.text', " Minimum 10 characters")
                    .invoke('attr', 'class')
                    .should('eq', "password-checking col-md-12 unfinished")
            })
    })
    it("Verify should check the if the following contains with a lowercase, upppercase, number and special character", function() { 
        const select_array_strings = ['abcdefghijklmnopqrstuvwxyz', '0123456789', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '!@#$%^&*()'];
        let eight_random_strings = "";
        let password_length, select_chars;
        select_array_strings.forEach(string => {
            eight_random_strings += genPassword(password_length = 1, select_chars = string)
        })
        cy.get("#form-error-field > div.float-container > input")
            .type(eight_random_strings)
            .then(() => {
                cy.get("#showPassword > div.row.password-checking > div:nth-child(1) > div > div:nth-child(1) > div")
                    .should('have.text', " One lowercase character") //Verify the label
                    .invoke('attr', 'class')
                    .should('eq', "password-checking col-md-12 finished"); //Verify checkbox is enabled
            })
    })
    it("Verify the password checking for the  password will be disappear if the minimum required for password is supplied", function() { 
        const select_array_strings = ['abcdefghijklmnopqrstuvwxyz', '0123456789', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '!@#$%^&*()'];
        let ten_random_strings = "Qa";
        let password_length, select_chars;
        select_array_strings.forEach(string => {
            ten_random_strings += genPassword(password_length = 1, select_chars = string)
        })
        cy.get("#form-error-field > div.float-container > input")
            .type(ten_random_strings)
            .then(() => {
                cy.get("#showPassword > div.row.password-checking.ng-hide")
                    .invoke('attr', 'class')
                    .should('eq', "row password-checking ng-hide"); 
            })
    })
    it("Verify should display an error 'Required field' if the password field is empty ", function() { 
        const generate_password = Faker.internet.password(10);
        cy.get("#form-error-field > div.float-container > input")
            .type(generate_password)
            .clear()
            .then(() => {
                cy.get("#errorPassword > div")
                    .should('have.text', "Required field"); 
            })
    })
    it("Verify should  the valid input password should contain at least a lower case, a uppercase,  a number, a special character and minimum of 10 characters", function() { 
        const select_array_strings = ['abcdefghijklmnopqrstuvwxyz', '0123456789', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '!@#$%^&*()'];
        let ten_char_password= "";
        let password_length, select_chars;
        select_array_strings.forEach(string => {
            ten_char_password += genPassword(password_length = 2, select_chars = string)
        })
        ten_char_password = ten_char_password.slice(0,11) // retain the first 10 chars
        cy.get("#form-error-field > div.float-container > input")
            .type(ten_char_password) // retain the first 10 chars
            .then(() => {
                cy.get("#showPassword > div.row.password-checking.ng-hide")
                    .invoke('attr', 'class')
                    .should('eq', "row password-checking ng-hide"); 
            })
    })
    it("Verify should the checkbox for the label 'I'm not a robot' should be clickable", function() { 
        const email = Faker.internet.email();
        const valid_mobile_no = Faker.phone.phoneNumber('+48##########');
        const select_array_strings = ['abcdefghijklmnopqrstuvwxyz', '0123456789', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '!@#$%^&*()'];
        let ten_char_password= "";
        let password_length, select_chars;
        select_array_strings.forEach(string => {
            ten_char_password += genPassword(password_length = 2, select_chars = string)
        })
        ten_char_password = ten_char_password.slice(0,11) // retain the first 10 chars
        // cy.intercept("GET", "/api/email-verifier?email=*").as("getEmailVerify");
        cy.get("input[id='email']")
            .type(email);
            
        cy.get("input[id='su-mobile']")
            .type(valid_mobile_no)

        cy.get("#form-error-field > div.float-container > input")
                .type(ten_char_password) // retain the first 10 chars
        // cy.wait("@getEmailVerify").then(() => {

        cy.window().then(win => {
            win.document
                .querySelector("#form-error-response > div.g-recaptcha.ng-isolate-scope > div > div > iframe")
                .contentDocument.getElementById("recaptcha-token")
                  .click();
              });
    })
    it("Verify all mandatory fields will be display error if submit button is click ", function() { 
       cy.get("input[type='submit']")
        .click()
       .then(() => {
            cy.get("#errorEmail3 > div")
                .should('have.text', "Email Address is required"); //Email is missing
            cy.get("#errorMobile > div")
                .should('have.text', "Mobile Number is required"); //Mobile no is missing
            cy.get("#errorPassword > div")
                .should('have.text', "Password is required"); //Password is missing
            cy.get("#errorRecaptcha > div")
                .should('have.text', "Captcha is required"); //Captcha is missing
       })
    })
})

function imageToBase64Converter(url, callback) {
    let xhRequest = new XMLHttpRequest();
    xhRequest.onload = function () {
      let reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhRequest.response);
    };
    xhRequest.open('GET', url);
    xhRequest.responseType = 'blob';
    xhRequest.send();
  }

function genPassword(password_length, select_chars) {
    let passwordLength = password_length;
    let random_password = "";
 for (let i = 0; i <= passwordLength; i++) {
   let randomNumber = Math.floor(Math.random() * select_chars.length);
   random_password += select_chars.substring(randomNumber, randomNumber +1);
  }
    return random_password;
 }




